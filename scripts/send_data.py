#!/usr/bin/env python

# This runs on the robot. Reads laser data and transformations and send to the cloud using rosbridge. Also reads transformations from the cloud.

import time
import rospy
import roslibpy
import roslibpy.tf
import tf2_ros
from sensor_msgs.msg import LaserScan
import geometry_msgs.msg 
from nav_msgs.msg import OccupancyGrid

global laser_data
laser_data=LaserScan()
global mapa 
mapa = OccupancyGrid()
global got_new_map
got_new_map = bool()


# Laser Callback - get scan data from the robot (to be sent to the mapper using rosbridge)
def callback(msg):
    	global laser_data
    	laser_data = msg
	#laser_data.header.frame_id='/base_scan'

# TF callback - get TF from rosbridge
def tf_map_odom_cb(msg):
	tb=tf2_ros.TransformBroadcaster()
        #print("got a map->odom transform.")
	t = geometry_msgs.msg.TransformStamped()
    	t.header.stamp = rospy.Time.now()
    	t.header.frame_id = "map"
    	t.child_frame_id = "odom"
    	t.transform.translation.x = msg['translation']['x']
    	t.transform.translation.y = msg['translation']['y']
    	t.transform.translation.z = msg['translation']['z']
        t.transform.rotation.x = msg['rotation']['x']
    	t.transform.rotation.y = msg['rotation']['y']
    	t.transform.rotation.z = msg['rotation']['z']
    	t.transform.rotation.w = msg['rotation']['w']
	tb.sendTransform(t)

# Map callback - receive the map from rosbridge      
def receive_cb(msg):
	global mapa
	global got_new_map
	print("Map Received!")
	print(msg['header'])
	print(msg['header']['stamp']['secs'])
	mapa.header.stamp.secs 	= msg['header']['stamp']['secs']
	mapa.header.stamp.nsecs = msg['header']['stamp']['nsecs']
	mapa.header.frame_id = msg['header']['frame_id']
	mapa.header.seq = msg['header']['seq']
        mapa.info.map_load_time.secs = msg['info']['map_load_time']['secs']
	mapa.info.map_load_time.nsecs = msg['info']['map_load_time']['nsecs']
	mapa.info.resolution = msg['info']['resolution']
	mapa.info.width = msg['info']['width']
	mapa.info.height = msg['info']['height']
	mapa.info.origin.position.x = msg['info']['origin']['position']['x']
	mapa.info.origin.position.y = msg['info']['origin']['position']['y']
	mapa.info.origin.position.z = msg['info']['origin']['position']['z']
	mapa.info.origin.orientation.x = msg['info']['origin']['orientation']['x']
	mapa.info.origin.orientation.y = msg['info']['origin']['orientation']['y']
	mapa.info.origin.orientation.z = msg['info']['origin']['orientation']['z']
	mapa.info.origin.orientation.w = msg['info']['origin']['orientation']['z']
	mapa.data = msg['data']
	got_new_map = True

	
#Function that create the messages to be sent to rosbridge - they use json format
# sensor_msgs/LaserScan
def LaserScanMsg(seq, stamp, frame_id, angle_min, angle_max, angle_increment, time_increment, scan_time, range_min, range_max, ranges, intensities):
	return dict({'header': dict({'seq': seq, 'stamp': stamp.to_sec(), 'frame_id': frame_id}), 'angle_min': angle_min, 'angle_max': angle_max, 'angle_increment': angle_increment, 'time_increment': time_increment, 'scan_time': scan_time, 'range_min': range_min, 'range_max': range_max, 'ranges': ranges, 'intensities': intensities})

# sensor_msgs/LaserScan
def LaserScanMsg1(laser_data):
	return dict({'header': dict({'seq': laser_data.header.seq, 'stamp': laser_data.header.stamp.to_sec(), 'frame_id': laser_data.header.frame_id}), 'angle_min': laser_data.angle_min, 'angle_max': laser_data.angle_max, 'angle_increment': laser_data.angle_increment, 'time_increment': laser_data.time_increment, 'scan_time': laser_data.scan_time, 'range_min': laser_data.range_min, 'range_max': laser_data.range_max, 'ranges': laser_data.ranges, 'intensities': laser_data.intensities})

#geometry_msgs/TransformStamped
def TfMsg(seq, stamp, frame_id, child_frame_id, x, y, z, qx, qy, qz, qw):
	return dict({'header': dict({'seq': seq, 'stamp': stamp, 'frame_id': frame_id}), 'child_frame_id': child_frame_id, 'transform': dict({'translation': dict({'x': x, 'y': y, 'z': z}), 'rotation': dict({'x': qx, 'y': qy, 'z': qz, 'w': qw})})})

#geometry_msgs/TransformStamped
def TfMsg1(Trans):
	return dict({'header': dict({'seq': Trans.header.seq, 'stamp': Trans.header.stamp.to_sec(), 'frame_id': Trans.header.frame_id}), 'child_frame_id': Trans.child_frame_id, 'transform': dict({'translation': dict({'x': Trans.transform.translation.x, 'y': Trans.transform.translation.y, 'z': Trans.transform.translation.z}), 'rotation': dict({'x': Trans.transform.rotation.x, 'y': Trans.transform.rotation.y, 'z': Trans.transform.rotation.z, 'w': Trans.transform.rotation.w})})})


# Main function
def main():
	rospy.init_node('send_data')

	rate = rospy.Rate(20) 

	global laser_data
	global mapa
	global got_new_map

	got_new_map = False

	# Subscribe to laser locally	
	sub = rospy.Subscriber('/scan', LaserScan, callback)
	
	# Subscribe to TF locally
	tfBuffer = tf2_ros.Buffer()
    	listener = tf2_ros.TransformListener(tfBuffer)

        # Server parameters
	IP = rospy.get_param('~IP', 'localhost')
	PORT = rospy.get_param('~PORT', 8080)
	rospy.loginfo("Server at IP: %s, Port: %d", IP, PORT)

	# Configure and start local client
        client = roslibpy.Ros(host=IP, port=PORT)
	client.run()

	# Subscribe to TF remotelly -> map -> odom is the return of SLAM/Localization algorithm
	tf_client = roslibpy.tf.TFClient(client, fixed_frame='map', angular_threshold=0.0, translation_threshold=0.0, rate=10)
        tf_client.subscribe('odom', tf_map_odom_cb)
        
	# Remote publishers 
	talker1 = roslibpy.Topic(client, '/scan', 'sensor_msgs/LaserScan')
	talker2 = roslibpy.Topic(client, '/transformation1', 'geometry_msgs/TransformStamped')
        talker3 = roslibpy.Topic(client, '/transformation2', 'geometry_msgs/TransformStamped')

	# Subscribe to the map topic remotelly
	#listener = roslibpy.Topic(client, '/map', 'nav_msgs/OccupancyGrid')
	#listener.subscribe(receive_cb)
	
	# Local publisher to the received map 
	pub = rospy.Publisher('map', OccupancyGrid, queue_size=1)

	while (client.is_connected) & (not rospy.is_shutdown()):

		# Publishes laser through rosbridge
		message1=LaserScanMsg1(laser_data)
		talker1.publish(message1)

		# Publishes base_link -> odom through rosbridge
		try:
            		trans = tfBuffer.lookup_transform('odom', 'base_link', rospy.Time())
        	except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
            		rate.sleep()
            		continue
		message2=TfMsg1(trans)
		talker2.publish(message2)


		# Publishes base_laser -> base_link (This is static. We may not need it here)
		try:
            		trans = tfBuffer.lookup_transform('base_link', 'base_laser', rospy.Time())
			trans.header.stamp = laser_data.header.stamp
        	except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException):
            		rate.sleep()
            		continue
		message3=TfMsg1(trans)
		talker3.publish(message3)

    		rospy.loginfo("Sending messages...")

		if (got_new_map):
			 pub.publish(mapa)
			 got_new_map = False

    		rate.sleep()

	talker1.unadvertise()
	talker2.unadvertise()
	talker3.unadvertise()
	tf_client.dispose()
	client.terminate()

if __name__ == '__main__':
    main()
