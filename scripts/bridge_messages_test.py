import time
import rospy
import roslibpy
from sensor_msgs.msg import LaserScan


global laser_data
laser_data=LaserScan()

def callback(msg):
    	global laser_data
    	laser_data = msg
	laser_data.header.frame_id='/base_scan'
	#print ('Reading data')

# geometry_msgs/Vector3
def Vector3Msg(x, y, z):
	#return roslibpy.Message({'x': x, 'y': y, 'z': z})
	return dict({'x': x, 'y': y, 'z': z})

# geometry_msgs/Twist
def TwistMsg(vx, vy, vz, wx, wy, wz):
	return dict({'linear': Vector3(vx, vy, vz), 'angular': Vector3(wx, wy, wz)})

# geometry_msgs/Twist
def Twist2Msg(v, w):
	return dict({'linear': v, 'angular': w})

# std_msgs/Header
def HeaderMsg(seq, stamp, frame_id):
	return dict({'seq': seq, 'stamp': stamp, 'frame_id': frame_id})

# sensor_msgs/LaserScan
def LaserScanMsg(seq, stamp, frame_id, angle_min, angle_max, angle_increment, time_increment, scan_time, range_min, range_max, ranges, intensities):
	return dict({'header': dict({'seq': seq, 'stamp': stamp.to_sec(), 'frame_id': frame_id}), 'angle_min': angle_min, 'angle_max': angle_max, 'angle_increment': angle_increment, 'time_increment': time_increment, 'scan_time': scan_time, 'range_min': range_min, 'range_max': range_max, 'ranges': ranges, 'intensities': intensities})

# sensor_msgs/LaserScan
def LaserScanMsg1(laser_data):
	return dict({'header': dict({'seq': laser_data.header.seq, 'stamp': laser_data.header.stamp.to_sec(), 'frame_id': laser_data.header.frame_id}), 'angle_min': laser_data.angle_min, 'angle_max': laser_data.angle_max, 'angle_increment': laser_data.angle_increment, 'time_increment': laser_data.time_increment, 'scan_time': laser_data.scan_time, 'range_min': laser_data.range_min, 'range_max': laser_data.range_max, 'ranges': laser_data.ranges, 'intensities': laser_data.intensities})

#geometry_msgs/TransformStamped
def TfMsg(seq, stamp, frame_id, child_frame_id, x, y, z, qx, qy, qz, qw):
	return dict({'header': dict({'seq': seq, 'stamp': stamp, 'frame_id': frame_id}), 'child_frame_id': child_frame_id, 'transform': dict({'translation': dict({'x': x, 'y': y, 'z': z}), 'rotation': dict({'x': qx, 'y': qy, 'z': qz, 'w': qw})})})

def main():
	global laser_data
	rospy.init_node('send_laser')
	sub = rospy.Subscriber('/world', LaserScan, callback)
	rate = rospy.Rate(20) 
	
        client = roslibpy.Ros(host='localhost', port=8080)
	#client = roslibpy.Ros(host='3.90.4.131', port=8080)
	client.run()

	talker3 = roslibpy.Topic(client, '/scan2', 'sensor_msgs/LaserScan')
	talker = roslibpy.Topic(client, '/Twist', 'geometry_msgs/Twist')
	talker1 = roslibpy.Topic(client, '/Vector3', 'geometry_msgs/Vector3')
	talker2 = roslibpy.Topic(client, '/Header', 'std_msgs/Header')
	talker4 = roslibpy.Topic(client, '/odometry', 'geometry_msgs/TransformStamped')
	

	while (client.is_connected) & (not rospy.is_shutdown()):
		a=3.5
		#message=roslibpy.Message({'x': a, 'y': 1.0, 'z': 2.0})
		#message=dict({'x': a, 'y': 1.0, 'z': 2.0})
		message=Vector3Msg(2, 5, 1)
		#message1 = Twist(1.0, 2.0, 3.0, 4.0, 5.0, 6.0)
		message1=Twist2Msg(message, message)
		#print(message1)
		talker1.publish(message)
    		talker.publish(message1)
		talker2.publish(HeaderMsg(1, rospy.get_time(), '/world'))
		message3=LaserScanMsg1(laser_data)
		talker3.publish(message3)

		message4=TfMsg(1, rospy.get_time(), '/world', '/gui', 1, 2, 3, 4, 5, 6, 7 )
		print(message4)
		talker4.publish(message4)

    		print('Sending message...')
    		rate.sleep()
	talker.unadvertise()
	client.terminate()

if __name__ == '__main__':
    main()
