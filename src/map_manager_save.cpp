
#include <ros/ros.h>
#include "slam_toolbox_msgs/SerializePoseGraph.h"
#include <time.h>
#include <unistd.h>


int main(int argc, char** argv)
{
  ros::init(argc, argv, "map_manager");
  ros::NodeHandle nh("~");

  char buff[FILENAME_MAX]; //create string buffer to hold the current folder path
  getcwd( buff, FILENAME_MAX );
  std::string current_working_dir(buff); // current folder path
  std::string map_name;

  // Check if the map name was passed
  if ( argc == 2 ){
     map_name = current_working_dir+"/"+argv[1];
  }
  else{
     time_t t = time(0);   // get time now
     struct tm * now = localtime( & t );
     char buffer [80];
     strftime (buffer,80,"%Y-%m-%d-%H-%M-%S",now);
     map_name = current_working_dir+"/"+buffer;
  }
     
  ROS_INFO(" Saving %s ...", map_name.c_str());

  ros::ServiceClient serialize = nh.serviceClient<slam_toolbox_msgs::SerializePoseGraph>("/slam_toolbox/serialize_map");
  //ros::ServiceClient saveMap = nh.serviceClient<slam_toolbox_msgs::SaveMap>("/slam_toolbox/save_map");
  

  slam_toolbox_msgs::SerializePoseGraph msg1;
  msg1.request.filename = map_name;
  if (!serialize.call(msg1))
  {
    ROS_WARN("SlamToolbox: Failed to serialize pose graph to file, is service running?");
  }
  else
  {
    ROS_INFO("SlamToolbox: Map serialized!");
  }

  /*slam_toolbox_msgs::SaveMap msg2;
  msg2.request.name.data = "/home/g.pereira/catkin_ws/map1";
  if (!saveMap.call(msg2))
  {
    ROS_WARN("SlamToolbox: Failed to save map, is service running?");
  }
  else
  {
    ROS_INFO("SlamToolbox: Map saved!");
  }*/

  ros::spinOnce();
  return 0;
	

}
