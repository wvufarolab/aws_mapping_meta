#include <ros/ros.h>
#include "slam_toolbox/toolbox_msgs.hpp"
#include "sensor_msgs/LaserScan.h"
#include <unistd.h>
#include <sys/stat.h>

ros::Duration time_between_scans(0);
ros::Time time_previous_scan;
bool got_first_scan;

void laserCB(const sensor_msgs::LaserScan::ConstPtr& scan)
{
      if (!got_first_scan){
	 time_previous_scan = scan->header.stamp;
	 got_first_scan = true;
      }	
      time_between_scans =  time_previous_scan-scan->header.stamp;
      time_previous_scan = scan->header.stamp;	 
}


int main(int argc, char** argv)
{
  ros::init(argc, argv, "map_manager");
  ros::NodeHandle nh("~");
  ros::ServiceClient serialize = nh.serviceClient<slam_toolbox_msgs::SerializePoseGraph>("/slam_toolbox/serialize_map");
  ros::Subscriber sub = nh.subscribe("/scan",1, laserCB);

  ros::Rate loop_rate(10);
  got_first_scan = false;

  char buff[FILENAME_MAX]; //create string buffer to hold the current folder path
  getcwd( buff, FILENAME_MAX );
  std::string current_working_dir(buff); // current folder path
  std::string map_name;

  int cont = 0;
  struct stat buffer;

  while (ros::ok()){


     if (time_between_scans.toSec() > 5){
 
	do {
            map_name = current_working_dir+"/map"+std::to_string(cont);
	    cont++;	 
        }
  	while (stat ((map_name+".data").c_str(), &buffer) == 0);        	
     
  	ROS_INFO(" Saving %s ...", map_name.c_str());

  	slam_toolbox_msgs::SerializePoseGraph msg1;
  	msg1.request.filename = map_name;
  	if (!serialize.call(msg1))
  	{
    		ROS_WARN("SlamToolbox: Failed to serialize pose graph to file, is service running?");
  	}
  	else
  	{
    		ROS_INFO("SlamToolbox: Map serialized!");
  	}

     }
     
       
     ros::spinOnce();
     loop_rate.sleep();
  } // while

  return 0;
	

}
