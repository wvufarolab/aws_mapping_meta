
#include <ros/ros.h>
#include "slam_toolbox_msgs/DeserializePoseGraph.h"
#include <unistd.h>
#include <sys/stat.h>


int main(int argc, char** argv)
{

  ros::init(argc, argv, "map_manager");
  ros::NodeHandle nh("~");
  
  // Check if the map name was passed
  if ( argc != 2 ){
        ROS_ERROR("Usage %s <map_name_without_extension>", argv[0]);
	return 1;
  }

  char buff[FILENAME_MAX]; //create string buffer to hold the current folder path
  getcwd( buff, FILENAME_MAX );
  std::string current_working_dir(buff); // current folder path
  std::string file_name = current_working_dir+"/"+argv[1]; // file name with path
 
  // Check if the map exists in the current folder
  struct stat buffer;   
  if (stat ((file_name+".data").c_str(), &buffer) != 0){
        ROS_ERROR("Map named %s not found!", file_name.c_str());
	return 1;
  }
         
  ROS_INFO(" Loading %s...", file_name.c_str());

  ros::ServiceClient load_map = nh.serviceClient<slam_toolbox_msgs::DeserializePoseGraph>("/slam_toolbox/deserialize_map");

  typedef slam_toolbox_msgs::DeserializePoseGraph::Request procType;

  slam_toolbox_msgs::DeserializePoseGraph msg;
  msg.request.filename = file_name;
 
  msg.request.match_type = procType::START_AT_FIRST_NODE;
 
  if (!load_map.call(msg))
  {
     ROS_ERROR("SlamToolbox: Failed to deserialize mapper object "
      "from file, is service running?");
  }
  else
  {
     ROS_INFO("Map loaded!");
  } 
  
  ros::spinOnce();

  return 0;	

}
